;( function( $, window, document, undefined ) {

  "use strict";
  
    // Use regular expressions found in https://foundation.zurb.com/sites/docs/abide.html
    var VALIDATORS = {
      alpha : /^[a-zA-Z]+$/,
      alpha_numeric : /^[a-zA-Z0-9]+$/,
      integer : /^[-+]?\d+$/,
      number : /^[-+]?\d*(?:[\.\,]\d+)?$/,
      // amex, visa, diners
      card : /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/,
      cvv : /^([0-9]){3,4}$/,
      // http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#valid-e-mail-address
      email : /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/,
      // abc.de
      domain : /^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,8}$/,
      datetime : /^([0-2][0-9]{3})\-([0-1][0-9])\-([0-3][0-9])T([0-5][0-9])\:([0-5][0-9])\:([0-5][0-9])(Z|([\-\+]([0-1][0-9])\:00))$/,
      // YYYY-MM-DD
      date : /(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))$/,
      // HH:MM:SS
      time : /^(0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9]){2}$/,
      dateISO : /^\d{4}[\/\-]\d{1,2}[\/\-]\d{1,2}$/,
      // MM/DD/YYYY
      month_day_year : /^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.]\d{4}$/,
      // DD/MM/YYYY
      day_month_year : /^(0[1-9]|[12][0-9]|3[01])[- \/.](0[1-9]|1[012])[- \/.]\d{4}$/,
      // #FFF or #FFFFFF
      color : /^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/
    };

		var pluginName = "versatileValidator",
			defaults = {
        HttpMethod: 'POST',
        DataServerUrl: null
      };

		function VersatileValidator ( element, options ) {
			this.element = element;

			this.settings = $.extend( {}, defaults, options );
			this._defaults = defaults;
			this._name = pluginName;
			this.init();
		}

		$.extend( VersatileValidator.prototype, {
			init: function() {
        var self = this,
            form = this.element,
            formBaseErrorMessage = this.element.querySelector('p.base-error-message'),
            formInputs, formSelects, formSubmitElem;

        if (form !== null && typeof form !== 'undefined') {
          var formInputs = $(form).find('input'),
            formSelects = $(form).find('select'),
            formSubmitElem = $(form).find('button:not([type=button]), input[type=submit]');

            // Prevent default behaviours for all inputs and select elements
            for (var i = 0; i < formInputs.length; i++) {
              if ($(formInputs[i]).attr('data-type') !== null &&
                  typeof $(formInputs[i]).attr('data-type') !== 'undefined') {
                if ($(formInputs[i]).attr('type') === 'text') {
                  if (VALIDATORS[$(formInputs[i]).attr('data-type')] !== null &&
                      typeof VALIDATORS[$(formInputs[i]).attr('data-type')] !== 'undefined') {
                    $(formInputs[i]).attr('pattern', VALIDATORS[$(formInputs[i]).attr('data-type')].toString().slice(1, -1));
                  }
                }
              }
              $(formInputs[i]).bind('invalid', function (event) {
                event.preventDefault();
              })
              $(formInputs[i]).bind('change', function (event) {
                if (event.target.checkValidity()) {
                  var parent  = event.target.parentNode,
                      errorMessage = event.target.parentNode.querySelector('p.error-message');
                  if (errorMessage !== null && typeof errorMessage !== 'undefined') {
                    parent.removeChild(errorMessage);
                  }
                } else {
                  var customErrorMessage = event.target.getAttribute('error-message')
                  event.target.parentNode.insertAdjacentHTML('beforeend', '<p class="error-message">' +
                  (customErrorMessage ? customErrorMessage : event.target.validationMessage) +
                  '</p>')
                }
              })
            }
            for (var i = 0; i < formSelects.length; i++) {
              $(formSelects[i]).bind('invalid', function (event) {
                event.preventDefault();
              })
              $(formSelects[i]).bind('change', function (event) {
                if (event.target.checkValidity()) {
                  var parent  = event.target.parentNode,
                      errorMessage = event.target.parentNode.querySelector('p.error-message');
                  if (errorMessage !== null && typeof errorMessage !== 'undefined') {
                    parent.removeChild(errorMessage);
                  }
                } else {
                  var customErrorMessage = event.target.getAttribute('error-message')
                  event.target.parentNode.insertAdjacentHTML('beforeend', '<p class="error-message">' +
                  (customErrorMessage ? customErrorMessage : event.target.validationMessage) +
                  '</p>')
                }
              })
            }

            // Show validations
            $(formSubmitElem).bind('click', function (event) {
              var invalidFields = $(form).find(':invalid'),
                errorMessages = $(form).find('.error-message'),
                parent;

              // Remove any existing messages
              for (var i = 0; i < errorMessages.length; i++) {
                errorMessages[i].parentNode.removeChild(errorMessages[i])
              }

              for (var i = 0; i < invalidFields.length; i++) {
                var customErrorMessage = invalidFields[i].getAttribute('error-message')
                parent = invalidFields[i].parentNode;
                parent.insertAdjacentHTML('beforeend', '<p class="error-message">' +
                  (customErrorMessage ? customErrorMessage : invalidFields[i].validationMessage) +
                  '</p>')
              }

              // If there are errors, give focus to the first invalid field
              if (invalidFields.length > 0) {
                invalidFields[0].focus()
              }
            })

            // Handle form submission to AJAX call instead
            $(form).bind('submit', function (event) {
              var currentForm = event.target;
              event.preventDefault();
              $(formBaseErrorMessage).text('');
              if (this.checkValidity()) {

                // If DataServerUrl is not provided, do not perform server side submission and validation
                if (self.settings.DataServerUrl === null ||
                    typeof self.settings.DataServerUrl === 'undefined') {
                  return false
                }
                
                $.ajax({
                  type: 'POST',
                  async: true,
                  contentType: false,
                  cache: false,
                  processData: false,
                  data: new FormData(),
                  url: self.settings.DataServerUrl
                }).done(function(response) {
                  var errorMessages = $(form).find('.error-message');

                  // Remove any existing messages
                  for (var i = 0; i < errorMessages.length; i++) {
                    errorMessages[i].parentNode.removeChild(errorMessages[i]);
                  }

                  // Capture server-sided validation messages
                  if (response) {
                    if (response.status === false) {
                      for (var i = 0; i < response.messages.length; i++) {
                        var serverErrorFieldName = response.messages[i].name,
                          serverErrorMessage = response.messages[i].message,
                          errorFieldElem = currentForm.querySelector('[name="' + serverErrorFieldName + '"]');

                        if (errorFieldElem) {
                          errorFieldElem.parentNode.insertAdjacentHTML('beforeend', '<p class="error-message">' +
                            serverErrorMessage +
                            '</p>');
                        }
                      }
                    }
                  }
                }).fail(function(error) {
                  $(formBaseErrorMessage).text('Network connectivity problem detected.');
                });

              }
              return false;
            })
        }
			}
		} );

		$.fn[ pluginName ] = function( options ) {
			return this.each( function() {
				if ( !$.data( this, "plugin_" + pluginName ) ) {
					$.data( this, "plugin_" +
						pluginName, new VersatileValidator( this, options ) );
				}
			} );
		};

} )( jQuery, window, document );
